#!/bin/bash

#Combining altogether the csv datasets

#sed 1d final_dataset_vm_fabri.csv >fab.csv #ignoring first line
#sed 1d final_dataset_vm_max.csv > max.csv
#sed 1d partial_dataset_vm.csv > partial.csv

#of course would be memorywise better some awk but meh
#cat final_dataset_vm_dario.csv fab.csv max.csv partial.csv > final_csv.csv


#check for endline and add


#sed -i -e '$a\' *.csv

#faster
for filename in ./*.csv; do 
	echo "processing $filename"
	[ -n "$(tail -c1 $filename)" ] && echo "adding newline character to the end of $filename " && echo >> "$filename"
done

#better
cat final_dataset_vm_dario.csv <(tail +2 final_dataset_vm_fabri.csv) <(tail +2 final_dataset_vm_max.csv) <(tail +2 partial_dataset_vm.csv) > final_csv.csv
